<!DOCTYPE html>
<html lang="en">

<head>
    <title>MyPresence | Riwayat Absen</title>
    @include('template.head')
    <link rel="stylesheet" href="css/style.css">
</head>


<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Main Sidebar Container -->
        @include('template.left-sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0" style="color: white;">Riwayat Absen</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Starter Page</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div><!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col">
                            <ul class="list-group">
                                <div class="panel-body">
                                    <form action="/create" method="get">
                                        <table class="table table-striped table-hover"
                                            style="border-radius: 25px; background-color: rgba(255, 255, 255, 0.08); backdrop-filter: blur(12px); color: white;">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>NIS</th>
                                                    <th>Nama</th>
                                                    <th>Tanggal</th>
                                                    <th>Absen</th>
                                                    <th>Keterangan</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($data as $d)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{ $d->nis }}</td>
                                                    <td>{{ $d->nama }}</td>
                                                    <td>{{ $d->tanggal }}</td>
                                                    <td>{{ $d->absen }}</td>
                                                    <td>{{ $d->keterangan }}</td>
                                                </tr>
                                                @empty
                                                <tr>

                                                </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </form>
                                </div>
                            </ul>
                        </div><!-- /.col-md-6 -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div><!-- /.content -->
        </div><!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>
        <!-- /.control-sidebar -->
        @include('template.footer')
        <!-- REQUIRED SCRIPTS -->
        @include('template.script')
</body>

</html>