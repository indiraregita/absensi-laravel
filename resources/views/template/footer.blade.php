<footer class="main-footer sidebar-dark-primary">
    <strong>Copyright &copy; 2014-2021 <a href="https://mypresence.com">MyPresence.com</a>.</strong> All rights
    reserved.
    <!-- To the right -->
    <div class="float-right sidebar-dark-primary">
        My Presence
    </div>
    <!-- Default to the left -->
</footer>