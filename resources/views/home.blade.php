<!DOCTYPE html>
<html lang="en">

<head>
  <title>MyPresence | Home</title>
  <!-- Embedding Poppins Font From Google Font -->
  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300&display=swap" rel="stylesheet">

  <!-- Embedding Icons From Fontawesome Icons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" />
  <link rel="stylesheet" href="{{ asset('background/style.css') }}">
  @include('template.head')
</head>


<body class="hold-transition sidebar-mini">
  <div class="wrapper">

    <!-- Main Sidebar Container -->
    @include('template.left-sidebar')

    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header sidebar-dark-primary">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Profile</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item active">Home</li>
                <li class="breadcrumb-item active">MyPresence</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
      <div class="background">
        <div class="biodata">
          <img src="img/default.png">
          <h3>{{Auth::user()->name}}</h3>
          <h6>NIS : {{ Auth::user()->nis }}</h6>
          <div class="biodata-list">
            <div class="biodataku">
              <span class="jd-biodata">TTL</span>
              <span>{{ Auth::user()->ttl }}</span>
            </div>
            <div class="biodataku">
              <span class="jd-biodata">Kelas</span>
              <span>{{ Auth::user()->kelas }}</span>
            </div>
            <div class="biodataku">
              <span class="jd-biodata">Sekolah</span>
              <span>{{ Auth::user()->sekolah }}</span>
            </div>
            <div class="biodataku">
              <span class="jd-biodata">Email</span>
              <span>{{ Auth::user()->email }}</span>
            </div>
          </div>
          <div class="edit">
            <a href="{{ route('profile.edit') }}" class="btn btn-primary">Edit Profile</a>
          </div>
        </div><br>
      </div>
    </div>
    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
      <div class="p-3">
        <h5>Title</h5>
        <p>Sidebar content</p>
      </div>
    </aside>

    <!-- ./wrapper -->

    <!-- Main Footer -->
    @include('template.footer')


    @include('template.script')

  </div>
  >>>>>>> 8ce67d00a3c6d572d8d4da0c21c044a3def2261b
</body>

</html>