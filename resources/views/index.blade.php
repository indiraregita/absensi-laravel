<!DOCTYPE html>
<html lang="en">

<head>
    <title>MyPresence | Absen</title>
    @include('template.head')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>


<body class="hold-transition sidebar-mini">
    <div class="wrapper">

        <!-- Main Sidebar Container -->
        @include('template.left-sidebar')

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header sidebar-dark-primary">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0" style="color: lightgray;">Absen</h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active">Starter Page</li>
                            </ol>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div><!-- /.content-header -->

            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col">
                            <div class="container">
                                <div style="text-align: center; color: white;">
                                    <h1 class="judul">ABSEN HARIAN</h1>
                                </div>
                                <div class="jam-digital">
                                    <div class="kotak">
                                        <p id="jam"></p>
                                    </div>
                                    <div class="kotak">
                                        <p id="menit"></p>
                                    </div>
                                    <div class="kotak">
                                        <p id="detik"></p>
                                    </div>
                                </div>
                                <div class="btn" style="text-align: center;">
                                    <a href="/create" class="btn btn-info btn-lg">Absen!</a>
                                </div>
                            </div>

                            <script>
                            window.setTimeout("waktu()", 1000);

                            function waktu() {
                                var waktu = new Date();
                                setTimeout("waktu()", 1000);
                                document.getElementById("jam").innerHTML = waktu.getHours();
                                document.getElementById("menit").innerHTML = waktu.getMinutes();
                                document.getElementById("detik").innerHTML = waktu.getSeconds();
                            }
                            </script>
                        </div>
                        <!-- /.col-md-6 -->
                    </div>
                    <!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
            <div class="p-3">
                <h5>Title</h5>
                <p>Sidebar content</p>
            </div>
        </aside>

        @include('template.script')

        <!-- REQUIRED SCRIPTS -->
        @include('template.script')
</body>

</html>