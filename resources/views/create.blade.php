<!DOCTYPE html>
<html lang="en">

<head>
  <title>MyPresence | Absen Harian</title>
  @include('template.head')
</head>


<body class="hold-transition sidebar-mini">
  <div class="wrapper">

    <!-- Navbar -->

    <!-- Main Sidebar Container -->
    @include('template.left-sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Absen</h1>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <div class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <form method="POST" action="\store">
                @csrf
                <div class="content-header">
                  <label for="nis">NIS</label>
                  <input type="number" class="form-control" id="nis" placeholder="Masukkan NIS" name="nis" autofocus required>
                </div>
                <div class="content-header">
                  <label for="nama">Nama</label>
                  <input type="text" class="form-control" id=" nama" placeholder="Masukkan Nama" name="nama" autofocus required>
                </div>
                <div class="content-header">
                  <label for="date">Tanggal</label>
                  <input type="date" class="form-control" id="date" name="tanggal" autofocus required>
                </div>
                <div class="content-header">
                  <label for="absen">Absen</label>
                  <input type="text" class="form-control" id="absen" placeholder="Masukkan Nomor Absen" name="absen" autofocus required>
                </div>
                <div class="content-header">
                  <label>Keterangan</label>
                  &nbsp;
                  <input type="radio" name="keterangan" value="Hadir" /> Hadir
                  &nbsp;
                  <input type="radio" name="keterangan" value="Sakit" /> Sakit
                  &nbsp;
                  <input type="radio" name="keterangan" value="Izin" /> Izin
                  &nbsp;
                  <br>
                </div>
                &nbsp;
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                  Absen!
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">MyPresence</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        Apakah Data Sudah Benar ?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Belum</button>
                        <input type="submit" name="submit" class="btn btn-primary" value="Sudah"></input>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              <br>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.col-md-6 -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->

  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->
  @include('template.script')

  @include('sweetalert::alert')
</body>

</html>