<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\AbsenController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PasswordController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

route::get('/', function () {
    return view('login/login');
});

// Home
route::get('/home', [HomeController::class, 'index'])->name('home');

// Login & Logout
route::get('/login', [LoginController::class, 'halamanlogin'])->name('login');
route::post('/postlogin', [LoginController::class, 'postlogin'])->name('postlogin');
route::get('/registrasi', [LoginController::class, 'registrasi'])->name('registrasi');
route::post('/saveregistrasi', [LoginController::class, 'saveregistrasi'])->name('saveregistrasi');
route::get('/logout', [LoginController::class, 'logout'])->name('logout');

// Profile
route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');

// Absen Harian & Absen Keseluruhan
route::get('/index', [AbsenController::class, 'index'])->name('index');
route::get('/create', [AbsenController::class, 'create'])->name('create');
route::post('/store', [AbsenController::class, 'store'])->name('store');
route::get('/riwayat', [AbsenController::class, 'riwayat'])->name('riwayat');

// Setting
route::group(['middleware' => 'auth'], function () {
    Route::get('password', [PasswordController::class, 'edit'])->name('user.password.edit');
    route::patch('password', [PasswordController::class, 'update'])->name('user.password.update');
});
