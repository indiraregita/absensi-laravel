<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePasswordRequest;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    public function index()
    {
        return view('/home');
    }

    public function password()
    {
        return view('password.edit');
    }

    public function riwayat()
    {
        return view('/riwayat');
    }

    /**
     * @param UpdatePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
}