<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends FormRequest
{
    public function authorize()
    {
        return Auth::check();
    }

    public function rules()
    {
        return [
            'name' => [
                'required', 'string', 'max:255'
            ],
            'nis' => [
                'required', 'string', 'max:255',
                Rule::unique('users', 'nis')->ignore(Auth::user()->id)
            ],
            'ttl' => [
                'required', 'string', 'max:255'
            ],
            'kelas' => [
                'required', 'string', 'max:255'
            ],
            'sekolah' => [
                'required', 'string', 'max:255'
            ],
            'email' => [
                'required', 'email', 'max:255',
                Rule::unique('users', 'email')->ignore(Auth::user()->id)
            ],
        ];
    }
}





