<?php

namespace App;

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Absen extends Model
{
    protected $table = "absen";
    protected $fillable = ['nis', 'nama', 'tanggal', 'absen', 'keterangan'];
    protected $guarded = [];
}